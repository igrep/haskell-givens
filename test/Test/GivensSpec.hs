{-# LANGUAGE OverloadedStrings #-}

module Test.GivensSpec
  ( spec
  ) where


import qualified Test.Hspec  as H

import           Test.Givens
import qualified Test.Givens as Givens


spec :: H.Spec
spec =
  H.describe "" $
    it "" $
      Givens.spec $
        describe "example group" $ do
          -- Equivalent with RSpec's `let!(:hoge){ 1 }`.
          -- An action passed to `given` function is executed every time
          -- an example (defined by `it`) in the `describe` block is executed,
          -- then the action's result is cached until the example's execution completes.
          hoge <- given $ return 1

          -- Equivalent with RSpec's `let(:hoge){ 1 }` (without exclamation).
          hogeIfNecessary <- givenIfNecessary $ runOnlyIfReferred

          -- To get the value of `given`, use `refer`.
          foo <- given ((+ 1) <$> refer hoge)

          -- Same as RSpec's `before(:each)`
          before $
            putStrLn =<< refer foo

          it "an example" $ do
            refer hoge `shouldReturn` 1

          it "another example" $ do
            refer foo `shouldReturn` 2
