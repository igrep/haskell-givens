{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Test.Givens
  ( spec
  , describe
  -- , context
  , it
  , Given
  , SpecM
  , given
  -- , givenAny
  -- , givenArbitrary
  , refer
  , before
  -- , beforeAll
  -- , shouldBe
  -- , shouldReturn
  -- , shouldSatisfy
  -- , expect
  -- , expect_
  , around
  , after
  -- , afterAll
  ) where

import           Control.Monad.IO.Class (MonadIO)
import           Control.Monad.Reader   (ReaderT)
import           Control.Monad.State    (State)
import qualified Data.IORef             as IOR
import           Data.Monoid            (Endo)
import qualified Data.Text              as T


spec :: SpecM a -> IO ()
spec _ =
  error "spec is not defined yet!"


describe :: T.Text -> SpecM ()
describe _ =
  error "describe is not defined yet!"


data Given a = Given
  { givenCache      :: IOR.IORef a
  , givenPreparator :: IO a
  }

newtype SpecM a = SpecM (State SpecState a) deriving (Functor, Applicative, Monad)

data SpecState = SpecState
  { specStateCurrentWrapper   :: Wrapper
  , specStateExpectationsTree :: ExpectationsTree
  }

data Wrapper = Wrapper
  { wrapperAround  :: Endo (IO ())
  , wrapperCleanUp :: IO ()
  }

data ExpectationsTree =
    Description T.Text [ExpectationsTree]
  | Expectation (ExpectationM ())

newtype ExpectationM a = ExpectationM (ReaderT ExpectationS IO a) deriving (Functor, Applicative, Monad, MonadIO)

type ExpectationS = IOR.IORef [ExpectationResult]

data ExpectationResult =
    ExpectationError
  | ExpectationSuccessful
  | ExpectationFailed
  deriving (Eq, Show, Ord)

data ExampleResult = ExampleSuccessful | ExampleSkipped | ExampleFailed deriving (Eq, Show, Ord)

type FailureDetail = T.Text


expectBase :: (a -> Maybe FailureDetail) -> IO a -> ExpectationM ()
expectBase _ =
  error "expectBase is not defined yet!"


given :: ExpectationM a -> SpecM (Given a)
given _ =
  error "given is not defined yet!"


before :: ExpectationM () -> SpecM ()
before _ =
  error "before is not defined yet!"


after :: ExpectationM () -> SpecM ()
after _ =
  error "after is not defined yet!"


around :: (ExpectationM () -> ExpectationM ()) -> SpecM ()
around _ =
  error "around is not defined yet!"


refer :: Given a -> ExpectationM a
refer _ =
  error "refer is not defined yet!"


it :: T.Text -> ExpectationM () -> SpecM ()
it _ =
  error "it is not defined yet!"
